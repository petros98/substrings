﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Substrings
{
    class Strings : IStrings
    {
        public string str;
        bool t = true;

        public List<int> _start_index = new List<int>();

         int count ;

        public Strings(string s)
        {
            str = s;
        }
        public void AllSubStrings()
        {
            string l;
            int k = 1;
            for (int i = 0; i < str.Length; i++)
            {
                for (int j = 0; j <= str.Length - k; j++)
                {
                    l = str.Substring(j, k);
                    Console.WriteLine(l);
                    count++;
                }
                k++;
            }
            Console.WriteLine("*****" + count + "*****");
        }       
        
        public void GetIndex(string sub1)
        {
            for (int i = 0; i < str.Length - sub1.Length; i++)
            {
                if (str[i] == sub1[0])
                {
                    for (int j = 0; j < sub1.Length; j++)
                    {
                        if (str[i + j] != sub1[j])
                        {
                            t = false;
                            break;
                        }

                    }
                    if (t)
                    {
                        _start_index.Add(i);
                    }
                }
                t = true;
            }
            if (_start_index.Count == 0)
            {
                Console.WriteLine("-1");
            }
            foreach (int item in _start_index)
            {
                Console.WriteLine(item);
            }
        }
    }
}
