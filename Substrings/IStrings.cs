﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Substrings
{
    interface IStrings
    {
        void AllSubStrings();
        void GetIndex(string sub1);
    }
}
